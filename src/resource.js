var res = {
    Runner_png : "res/images/Runner.png",
    RunningPath_png : "res/images/RunningPath.png",
    Platform_png : "res/images/Platform.png",
    Spike_png : "res/images/Spike.png",
    BGM_mp3: "res/sound/BGM.mp3",
    Dash_mp3: "res/sound/Dash.mp3",
    BoxDestroy_mp3: "res/sound/BoxDestroy.mp3",
    BounceOff_mp3: "res/sound/BounceOff.mp3",
    Booster0_png : "res/images/Booster0.png",
    Booster1_png : "res/images/Booster1.png",
    Booster2_png : "res/images/Booster2.png",
    Booster3_png : "res/images/Booster3.png",
    Booster4_png : "res/images/Booster4.png",
    Booster5_png : "res/images/Booster5.png",
    Booster6_png : "res/images/Booster6.png",
    Box_png : "res/images/Box1.png",
    Title_png : "res/images/Title.png"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}11111