var GameOver = cc.Sprite.extend({
	 ctor: function( ){
        this._super();
        this.initWithFile( "res/images/GameOver.png" );
        this.x = 300;
        this.y = 300;
        this.setPosition( cc.p( this.x, this.y ) );
    }
}
);