var Platform = cc.Sprite.extend({
	 ctor: function( x, y, type ){
        this._super();
        this.initWithFile( "res/images/Platform.png" );
        this.x = x;
        this.y = y;
        this.value = 100;
        this.contains = [];
        for (var i = 0; i >= 11; i++) {
        	this.contains.push(null);
        };
        this.setPosition( cc.p( this.x, this.y ) );
        if(type == 1){
            var generate = parseInt(Math.random()*3+Math.random()*3+Math.random()*3);
            if(generate<3)
                this.addBooster();
            else if(generate <6)
                this.addSpike();
            else
                this.addBox();
        }
        if(type == -1)
            this.addLauncher();
    },

    move: function( velo ) {
    	if(this.y>0)
        	this.setPositionY( this.y - velo );
    },

    getY: function(){
    	return this.y;
    },

    getContains: function(){
        return this.contains;
    },

    addSpike: function(){
    	var limit = parseInt(Math.random()*4)+parseInt(Math.random()*4+1);
    	for (var i = 0; i < limit; i++) {
        	var index = parseInt( Math.random()*9 );
        	if( this.contains[index] == null){
        		var temp = new Spike(index);
        		this.contains[index] = temp;
        		this.addChild(temp);
                this.value+=100;
        	}
        	else{
        		i--;
        	}
       };
    },

    addBooster: function(){
        var limit = parseInt(Math.random()*2)+parseInt(Math.random()*2)+1;
        for (var i = 0; i < limit; i++) {
            var index = parseInt( Math.random()*9 );
            if( this.contains[index] == null){
                var temp = new Booster(index,25);
                this.contains[index] = temp;
                this.addChild(temp);
                this.value+=50;
            }
            else{
                i--;
            }
       };
    },
    addBox: function(){
        for (var i = 0; i <= 8; i++) {
            var temp = new Box(i);
            this.contains[i] = temp;
            this.addChild(temp); 
            this.value+=50;
       };
   },

    addLauncher: function(){
        for (var i = 0; i <= 8; i++) {
            var temp = new Booster(i,25);
            this.contains[i] = temp;
            this.addChild(temp); 
       };
   },

   getValue:function(){
        return this.value;
   }
}
);