var Runner = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( "res/images/Runner0.png" );
        this.nextMove = Runner.DIR.STILL;
        this.move = Runner.DIR.STILL;
        this.lastMove = Runner.DIR.STILL;
        this.velocity = 0;
        this.acceleration = Runner.NORMAL_ACCELERATION;
        this.x = x;
        this.y = y;
        this.isDashingRight = false;
        this.isDashingLeft = false;
        this.isMovingRight = false;
        this.isMovingLeft = false;
        this.isDashing = false;
        this.isMoving = false;
        this.dashingFrame = 0;
        this.updatePosition();
        this.heat = 0;
        this.boost = 100; 
        this.isBoosting = false;
        this.isAlive = true;
        this.boostDelay = 0;
        this.movingAction = this.createAnimationAction();
    },
 
    update: function( dt ){
        if(config.delayStart >0){
            this.velocity = 5;
        }
        else if(this.isAlive){
        
            this.isDashing = this.isDashingRight || this.isDashingLeft;
            this.isMoving = this.isMovingRight || this.isMovingLeft;
            this.isBoosting = false;
        
            if(config.KEYS[cc.KEY.space]){
                this.boosting();
            }
            if(this.boost<100 && !this.isBoosting ) 
            {
                this.boost+=0.075;
            }
            if( config.KEYS[cc.KEY.w] && !this.isBoosting ){
                if(this.velocity<Runner.maxNormalVelocity)
                {
                    this.velocity += this.acceleration;
                    if(this.velocity>=Runner.maxNormalVelocity){
                        this.velocity = Runner.maxNormalVelocity;
                    }
                }
                else if(!this.isBoosting && this.velocity != Runner.maxNormalVelocity)
                    this.velocity -= 0.025;
                this.addYByVelocity();
            }

            if ( config.KEYS[cc.KEY.s]  && !this.isBoosting ){
                if(this.velocity>0)
                {
                    this.velocity -= this.acceleration*2;
                    this.heat += 0.4; 
                    if(this.heat>100) this.heat =100;
                }
                if(this.velocity< 0 )
                        this.velocity = 0;
                this.addYByVelocity();
            }

            if ( ( config.KEYS[cc.KEY.d] || this.isMovingRight ) && !this.isDashing && !this.isMovingLeft ){
                if ( !this.isMovingRight ) {
                    this.setFalseAllMoving()
                    this.isMovingRight = true;
                }
                var xVelocity = 5;
                this.dashingRight( xVelocity );
            }

            if ( ( config.KEYS[cc.KEY.a] || this.isMovingLeft ) && !this.isDashing && !this.isMovingRight){
                if ( !this.isMovingLeft ) {
                    this.setFalseAllMoving();
                    this.isMovingLeft = true;
                }
                var xVelocity = -5;
                this.dashingLeft( xVelocity );
            }

            if( ( config.KEYS[cc.KEY.right] || this.isDashingRight ) &&  !this.isDashingLeft && !this.isMoving ){
                if ( !this.isDashingRight ) {
                    this.setFalseAllMoving();
                    this.isDashingRight = true;
                }
                var xVelocity = 10;
                this.dashingRight( xVelocity );
            }

            if( ( config.KEYS[cc.KEY.left] || this.isDashingLeft ) && !this.isDashingRight && !this.isMoving ){
                if ( !this.isDashingLeft ) {
                    this.setFalseAllMoving();
                    this.isDashingLeft = true;
                }
                var xVelocity = -10;
                this.dashingLeft( xVelocity );
            }

            if(  !config.KEYS[cc.KEY.w] && !config.KEYS[cc.KEY.s] && !this.isDashing &&!this.isMoving && !this.isBoosting ){
                if(this.velocity > 0)
                    this.velocity -= Runner.SLOW_DOWN_ACCELERATION;
                else if(this.velocity < 0)
                    this.velocity += Runner.SLOW_DOWN_ACCELERATION;
                if(this.velocity<=0.1 && this.velocity>=-0.1)
                    this.velocity = 0;
                this.addYByVelocity();
            }
        
            if(this.dashingFrame>=10){
                this.setFalseAllMoving();
                this.dashingFrame =0;
            }
            this.updatePosition();

            
        }
        if (this.velocity>0) {
            this.runAction( this.movingAction );
        }

        else{
            this.stopAction( this.movingAction );
            this.initWithFile( "res/images/Runner0.png" );
        }
        if (this.heat>=100) {
            this.die();
        };

        if(this.boostDelay>0) this.boostDelay--;
        if( this.isBoosting ) this.heat += 0.1;     
        else if( this.heat>0&& this.velocity>=7) this.heat -= 0.01*this.velocity/5;
        if(this.heat>100) this.heat =100;
    },

    addYByVelocity: function(){
        //this.y += this.velocity;
    },

    updatePosition: function(){
        this.setPosition( cc.p( this.x, this.y ) );
    },

    setNextMove: function( mov ){
        this.nextMove = mov;
    },

    isNotThisDirection: function( mov ){
        return ! (( this.move == mov ) && this.dashingFrame!=0 ) 
    },

    getVelocity: function(){
        return this.velocity;
    },

    getHeat: function(){
        return this.heat;
    },

    getBoost: function(){
        return this.boost;
    },

    getX: function(){
        return this.x;
    },

    getY: function(){
        return this.y;
    },

    getBoostDelay : function(){
        return this.boostDelay;
    },

    setFalseAllMoving : function(){
            this.isDashingRight = false;
            this.isDashingLeft = false;
            this.isMovingRight = false;
            this.isMovingLeft = false;
    },

    boosting: function(){
        if(this.boostDelay===0){
            if(this.boost >0 && this.velocity<=Runner.maxBoostVelocity){
                this.isBoosting = true;
                this.velocity = Runner.maxBoostVelocity;
            }
            if(this.boost <0.3) this.boost = 0;
            else this.boost-=0.6;
            if(this.boost <= 0){
                this.isBoosting = false;
                this.boostDelay = 200; 
            }
            this.addYByVelocity();
            this.updatePosition();
        }
    },

    dashingRight: function( xVelocity ){
        var maxLimitOfDash = parseInt( this.x/100 ) * 100 +100;
        if (this.x < maxLimitOfDash && maxLimitOfDash <= Runner.RIGHT_BORDER ) {
           this.x =  this.x+xVelocity;
        }
        //this.y += this.velocity;
        this.dashingFrame++;
    },

    dashingLeft: function( xVelocity ){
        var minLimitOfDash = Math.ceil( this.x/100 ) * 100 - 100;
        if (this.x > minLimitOfDash && minLimitOfDash >= Runner.LEFT_BORDER ){
            this.x =  this.x+xVelocity;
            }
        //this.y += this.velocity;
        this.dashingFrame++;
    },

    die: function(){
        this.velocity = 0;
        this.isAlive = false;
        config.gameLayer.end();
    },

    boostByBooster: function(){
        this.velocity = 12.5;
    },

    bounceOff: function(){
        this.velocity = -1.25;
        this.heat += 2;
    },

    createAnimationAction: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( "res/images/Runner0.png" );
        animation.addSpriteFrameWithFile( "res/images/Runner1.png" );
        animation.addSpriteFrameWithFile( "res/images/Runner2.png" );
        animation.addSpriteFrameWithFile( "res/images/Runner3.png" );
        animation.addSpriteFrameWithFile( "res/images/Runner4.png" );
        animation.addSpriteFrameWithFile( "res/images/Runner5.png" );
        animation.addSpriteFrameWithFile( "res/images/Runner6.png" );
        animation.addSpriteFrameWithFile( "res/images/Runner7.png" );
        animation.addSpriteFrameWithFile( "res/images/Runner8.png" );
        animation.setDelayPerUnit( 0.025 );
        return cc.RepeatForever.create( cc.Animate.create( animation ) );
    }

});

Runner.NORMAL_ACCELERATION = 0.1;
Runner.SLOW_DOWN_ACCELERATION = 0.05;
Runner.SIDE_WAY_RUNNING = 2.5;
Runner.maxNormalVelocity = 7.5;
Runner.maxBoostVelocity = 10;
Runner.LEFT_BORDER = 200;
Runner.RIGHT_BORDER = 600;
Runner.DIR = {
	LEFT:1,
	RIGHT:2,
	UP:3,
	DOWN:4,
	RIGHT_DASH: 5,
	LEFT_DASH: 6,
	STILL: 0,
	CONST_VELOCITY : 7,
	BOOST: 8
}