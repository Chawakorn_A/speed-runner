var Title = cc.Sprite.extend({
	 ctor: function( ){
        this._super();
        this.initWithFile( "res/images/Title.png" );
        this.x = 300;
        this.y = 300;
        this.setPosition( cc.p( this.x, this.y ) );
    }
}
);