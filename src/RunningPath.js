var RunningPath = cc.Sprite.extend({
	 ctor: function( x, y ){
        this._super();
        this.initWithFile( "res/images/RunningPath.png" );
        this.x = x;
        this.y = y;
        this.setPosition( cc.p( this.x, this.y ) );
    }
}
);