var Box = cc.Sprite.extend({
	 ctor: function( x ){
        this._super();
        this.initWithFile( "res/images/Box1.png" );
        this.x = x*50+200;
        this.y = parseInt(Math.random()*50)+25;
        this.setPosition( cc.p( this.x, this.y ) );
        this.isEnable = true;
    },

    getX: function(){
    	return this.x;
    },

    getY: function(){
        return this.y;
    },

    activate: function(){
        if( this.isEnable)
        {
            if(config.player.getVelocity()>8){
                console.log("Go");
                cc.audioEngine.playEffect( res.BoxDestroy_mp3 );
                this.setVisible(false);
                this.isEnable = false;
            }
            else
            {
                cc.audioEngine.playEffect( res.BounceOff_mp3 );
                config.player.bounceOff();
            }
        }
    }
}
);