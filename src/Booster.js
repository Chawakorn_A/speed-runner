var Booster = cc.Sprite.extend({
    ctor: function( x,y ){
        this._super();
        this.initWithFile( "res/images/Booster1.png" );
        this.x = x*50+200;
        this.y = y;
        this.isActivated = false;
        this.setPosition( cc.p( this.x, this.y ) );
        this.movingAction = this.createAnimationAction();
        this.runAction( this.movingAction );
    },

    getX: function(){
    	return this.x;
    },

    getY: function(){
        return this.y;
    },

    activate: function(){
        if(!this.isActivated){
            cc.audioEngine.playEffect( res.Dash_mp3 );
            this.isActivated = true;
        }
        config.player.boostByBooster();
    },

    createAnimationAction: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( "res/images/Booster0.png" );
        animation.addSpriteFrameWithFile( "res/images/Booster1.png" );
        animation.addSpriteFrameWithFile( "res/images/Booster2.png" );
        animation.addSpriteFrameWithFile( "res/images/Booster3.png" );
        animation.addSpriteFrameWithFile( "res/images/Booster4.png" );
        animation.addSpriteFrameWithFile( "res/images/Booster5.png" );
        animation.addSpriteFrameWithFile( "res/images/Booster6.png" );
        animation.setDelayPerUnit( 0.05 );
        return cc.RepeatForever.create( cc.Animate.create( animation ) );
    }
}
);