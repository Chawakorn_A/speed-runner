var GameLayer = cc.LayerColor.extend({
    init: function(){


    	this.runningPath = new RunningPath(300,300);
    	this.addChild( this.runningPath );

        


    	this.gamePlatforms = [];
        for( var yPos =0 ; yPos<=1200 ; yPos+=100 ){
        	tempPlat = new Platform( 400, yPos, 0 );
        	this.gamePlatforms.push( tempPlat );
			this.runningPath.addChild( tempPlat );
		};

        this.randomPlatform = 0;
        this.delay = 25;

       	this.runner = new Runner( 400,200 );
        config.player = this.runner;
        this.runner.scheduleUpdate();
        this.runningPath.addChild( this.runner,2 ); 
        this.addKeyboardHandlers();
        this.velocityLabel = cc.LabelTTF.create( "Velocity : 0", "Impact", 20 );
		this.velocityLabel.setPosition( new cc.Point( 450, 20 ) );
		this.addChild( this.velocityLabel );

		this.heatLabel = cc.LabelTTF.create( "Heat : 000%", "Impact", 20 );
		this.heatLabel.setPosition( new cc.Point( 450, 80 ) );
		this.addChild( this.heatLabel );
        this.heatFrame = 0;
        
       
		this.boostLabel = cc.LabelTTF.create( "Boost : 000%", "Impact", 20 );
		this.boostLabel.setPosition( new cc.Point( 450, 50 ) );
		this.addChild( this.boostLabel,0 );

        this.readyLabel = cc.LabelTTF.create("","Impact",40);
        this.readyLabel.setPosition( new cc.Point( 300, 400 ) );
        this.addChild( this.readyLabel,0 );

        this.scoreTextLabel = cc.LabelTTF.create("SCORE : ","Impact",32);
        this.scoreTextLabel.setPosition( new cc.Point( 300, 550 ) );
        this.addChild( this.scoreTextLabel,0 );

        this.score = 0;
        this.scoreLabel = cc.LabelTTF.create("000000000","Impact",32);
        this.scoreLabel.setPosition( new cc.Point( 450, 550 ) );
        this.addChild( this.scoreLabel,0 );

		this.platformType = 0;
        config.gameLayer = this;
        cc.audioEngine.setMusicVolume(1);
        cc.audioEngine.stopMusic();
        this.scheduleUpdate();

        
    },

    update: function( dt ){
    	
        this.LabelUpdate();
    	for (var i = 0; i < this.gamePlatforms.length; i++) {
    		this.gamePlatforms[i].move( this.runner.getVelocity() );
    	};

    	this.createNewPlatform();

        this.score += parseInt(this.runner.getVelocity()*this.runner.getVelocity()/10);
        this.checkCollide();
   		this.readyCheck();
        cc.audioEngine.playMusic( res.BGM_mp3 );
        //cc.audioEngine.setMusicVolume(1);
        if(config.delayStart>0) this.score=0;

        config.score = this.score;
    },

    LabelUpdate:function(){
        var heat = this.pad( parseInt( this.runner.getHeat() ) , 3);
        var boost = this.pad( parseInt( this.runner.getBoost() ) , 3);
        var currentScore = this.pad( parseInt( this.score ), 9 );
        this.velocityLabel.setString( "Velocity : " + parseFloat(this.runner.getVelocity()).toFixed(2) );
        this.boostLabel.setString("boost : " + boost + "%");
        this.heatLabel.setString("Heat : " + heat + "%");
        this.scoreLabel.setString("" + currentScore);

        if( this.runner.getBoostDelay() != 0 ) this.boostLabel.setColor( cc.color(150,150,150) );
        else this.boostLabel.setColor( cc.color(255,255,255) );

        this.heatLabel.setColor( cc.color( parseInt( 255*heat/100 ),0,0 ) )
        if(heat>=85){
            if(this.heatFrame ===0) this.heatLabel.setVisible(!this.heatLabel.isVisible() );
            this.heatFrame = ++this.heatFrame%(100-heat+10);
        }
        else this.heatLabel.setVisible( true );
    },

    pad: function(num, size) {
    	var s = "00000000000" + num;
    	return s.substr(s.length-size);
	},

    addKeyboardHandlers: function() {
    	var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                config.KEYS[keyCode] = true;
            },
            onKeyReleased: function( keyCode, event ) {
                config.KEYS[keyCode] = false;
            }
        }, this);
    },

    createNewPlatform: function(){
        if(this.platformType === 0) this.randomPlatform = parseInt(Math.random()*3)+3;
    	if(this.gamePlatforms[0].getY()<25){
   			var temp = this.gamePlatforms[0];
            this.score += this.gamePlatforms[0].getValue();
   			this.gamePlatforms.shift();
   			this.runningPath.removeChild(temp,true);
            if(config.delayStart === 12 ){
                temp = new Platform( 400,this.gamePlatforms[this.gamePlatforms.length-1].getY()+100 , -1);
                config.delayStart--;
            }
   			else if(config.delayStart > 0 ) { 
                temp = new Platform( 400,this.gamePlatforms[this.gamePlatforms.length-1].getY()+100 , 0);
                config.delayStart--; 
            }
            else {
                temp = new Platform( 400,this.gamePlatforms[this.gamePlatforms.length-1].getY()+100 , this.platformType);
            }
            
   			this.platformType = ++this.platformType%this.randomPlatform;
   			this.gamePlatforms.push( temp );
			this.runningPath.addChild( temp );
   			this.gamePlatforms
   		}
    },

    checkCollide: function(){
        for (var i = 0; i < this.gamePlatforms.length; i++) {
            var playerYPos = this.runner.getY();
            var playerXPos = this.runner.getX();
            if(playerYPos >= this.gamePlatforms[i].getY()-75 && playerYPos <= this.gamePlatforms[i].getY()+75){
                var checkingPlatform = this.gamePlatforms[i];
                var obs = checkingPlatform.getContains();
                for (var j = 0; j < obs.length; j++) {
                    if( obs[j]!= null)
                        if( playerXPos>=obs[j].getX()-35 && playerXPos<=obs[j].getX()+35 ){
                            if(playerYPos>=obs[j].getY()+checkingPlatform.getY()-50-35 && playerYPos<=obs[j].getY()+checkingPlatform.getY()-50+35 )
                                obs[j].activate();
                        }
                };
            }
        };
    },

    readyCheck: function(){
        if(config.delayStart<10) this.readyLabel.setString("Ready...");
        if(config.delayStart<3 && config.delayStart>0) this.readyLabel.setString("GO!!");
        if(config.delayStart<=0) this.readyLabel.setString("");
    },

    end: function(){
        //this.update();
        cc.audioEngine.stopMusic( res.BGM_mp3 );
        cc.audioEngine.setMusicVolume(0);
        this.unscheduleUpdate();
        config.game.newGameOverMenu();
    }
    
});

var StartMenu = cc.LayerColor.extend({
    init: function(){
        this.menu = new Title();
        this.addChild( this.menu );

        menuButtons = cc.Menu.create();
        

        var label = cc.LabelTTF.create("START" , "Arial", 40);
        this.tmpBtn = cc.MenuItemLabel.create(label, function (e) {
            cc.log("Pressed");
            config.game.newGameLayer();
        }, this);

        this.tmpBtn.setPosition(50, 30);
        this.tmpBtn['data-tag'] = 0;

        menuButtons.addChild(this.tmpBtn,2,1);

        
        menuButtons.setPosition(350, 55);
        this.addChild(menuButtons, 1);          
    }

});

var GameOverMenu = cc.LayerColor.extend({
    init: function(){
        this.menu = new GameOver();
        this.addChild( this.menu );

        menuButtons = cc.Menu.create();
        
        var scoreLabel = cc.LabelTTF.create("YOUR SCORE : " + this.pad(config.score,8) , "Impact", 40);
        scoreLabel.setPosition(300, 450);
        this.addChild(scoreLabel, 0);

        var label = cc.LabelTTF.create("MAIN MENU" , "Arial", 30);
        this.tmpBtn = cc.MenuItemLabel.create(label, function (e) {
            cc.log("Pressed");
            config.game.reset();
        }, this);

        this.tmpBtn.setPosition(0, 0);
        this.tmpBtn['data-tag'] = 0;

        menuButtons.addChild(this.tmpBtn,2,1);

        
        menuButtons.setPosition(300, 195);
        this.addChild(menuButtons, 1);          
    },

    pad: function(num, size) {
        var s = "00000000000" + num;
        return s.substr(s.length-size);
    }

});

var StartScene = cc.Scene.extend({
        onEnter: function() {
            this._super();
            config.game = this;
            this.newStartMenu();
        },

        newStartMenu:function(){
            this.startMenu = new StartMenu();
            this.startMenu.init();
            this.addChild( this.startMenu );
        },

        newGameLayer:function(){
            this.layer = new GameLayer();
            this.layer.init();
            this.addChild( this.layer );
            this.removeChild(this.startMenu,true);
        },

        newGameOverMenu:function(){
            this.gameOverMenu = new GameOverMenu();
            this.gameOverMenu.init();
            this.addChild( this.gameOverMenu );
            this.removeChild(this.layer,true);
        },

        reset:function(){
            this.removeChild(this.gameOverMenu,true);
            this.resetConfig();
            this.newStartMenu();
        },

        resetConfig:function(){
            config.KEYS = [];
            config.player;
            config.delayStart = 25;
            config.score = 0;
        }
    }
);


