var Spike = cc.Sprite.extend({
	 ctor: function( x ){
        this._super();
        this.initWithFile( "res/images/Spike.png" );
        this.x = x*50+200;
        this.y = parseInt(Math.random()*50)+25;
        this.setPosition( cc.p( this.x, this.y ) );
    },

    getX: function(){
    	return this.x;
    },

    getY: function(){
        return this.y;
    },

    activate: function(){
        config.player.die();
        console.log("Die");
    }
}
);